package com.penginapan.penginapan;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

public class Login extends AppCompatActivity {
String xml="";
EditText txtpassword,txtusername;

    private ProgressDialog pDialog;
    JSONParser jsonParser = new JSONParser();

    private static final String TAG_SUKSES = "sukses";
    private static final String TAG_record = "record";
    private static final String TAG_xmlbio= "xmlbio";

    String xmlbio="";
    String ip="";

    String username,password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        ip=jsonParser.getIP();

        txtusername=(EditText)findViewById(R.id.edUser);
        txtpassword=(EditText)findViewById(R.id.edPass);

        Button btnLogin=(Button)findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
        public void onClick(View arg0) {

   			username=txtusername.getText().toString().trim();
   			password=txtpassword.getText().toString().trim();

   			if(username.length()<1){lengkapi("username");}
   		else if(password.length()<1){lengkapi("password");}
   		else {

                username = username.replaceAll(" ", "_");
                password = password.replaceAll(" ", "_");

                new ceklogin().execute();

            }
    }});


        Button btnRegistrasi=(Button)findViewById(R.id.btnRegistrasi);
        btnRegistrasi.setOnClickListener(new View.OnClickListener() {
        public void onClick(View arg0) {
          		Intent i = new Intent(Login.this,Registrasi.class);
            i.putExtra("pk", "");

     	          startActivity(i);
    }});


        Button btnForgot=(Button)findViewById(R.id.btnForgot);
        btnForgot.setOnClickListener(new View.OnClickListener() {
        public void onClick(View arg0) {
          		Intent i = new Intent(Login.this,Forgot.class);
     	          startActivity(i);
    }});

    }


    class ceklogin extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Login.this);
            pDialog.setMessage("Proses Login...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }
        protected String doInBackground(String... params) {
            int sukses;
            try {
                List<NameValuePair> myparams = new ArrayList<NameValuePair>();
                myparams.add(new BasicNameValuePair("username", username));
                myparams.add(new BasicNameValuePair("password", password));

                String url=ip+"android/login.php";
                Log.v("detail",url);
                JSONObject json = jsonParser.makeHttpRequest(url, "GET", myparams);
                Log.d("detail", json.toString());
                sukses = json.getInt(TAG_SUKSES);
                if (sukses == 1) {
                    JSONArray myObj = json.getJSONArray(TAG_record); // JSON Array
                    final JSONObject myJSON = myObj.getJSONObject(0);
                    runOnUiThread(new Runnable() {
                        public void run() {
                            try {
                                xmlbio=myJSON.getString(TAG_xmlbio);

                                Log.d("xmlbio", xmlbio);

                                Intent i = new Intent(getApplicationContext(),Menu_pengusaha.class);
                                i.putExtra("xmlbio", xmlbio);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(i);
                            }
                            catch (JSONException e) {e.printStackTrace();}
                        }});
                }
                else{
                    // jika id tidak ditemukan
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
        protected void onPostExecute(String file_url) {pDialog.dismiss();}
    }


        public void lengkapi(String item){
	    	new AlertDialog.Builder(this)
			.setTitle("Lengkapi Data")
			.setMessage("Silakan lengkapi data "+item)
			.setNeutralButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dlg, int sumthin) {
				}})
			.show();
	    }

	  public void sukses(String item,String ex){
	    	new AlertDialog.Builder(this)
			.setTitle("Sukses "+ex)
			.setMessage(ex+" data "+item+" Berhasil")
			.setNeutralButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dlg, int sumthin) {
					Intent i = new Intent(Login.this,Menu_pengusaha.class);
	     	        i.putExtra("xmlbio", xml);
					startActivity(i);
	     	         // finish();
				}})
			.show();
	    }

	  public void gagal(String item){
	    	new AlertDialog.Builder(this)
			.setTitle("Gagal Login")
			.setMessage("Login "+item+" ,, Gagal")
			.setNeutralButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dlg, int sumthin) {
				}})
			.show();
	    }


	  public boolean onKeyDown(int keyCode, KeyEvent event) {
	        if (keyCode == KeyEvent.KEYCODE_BACK) {
	        	finish();
	                return true;
	        }
	    return super.onKeyDown(keyCode, event);
	}  
        
	}  