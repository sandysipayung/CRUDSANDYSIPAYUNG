<?php
$pro = "simpan";
$tanggal = WKT(date("Y-m-d"));

if(!isset($_SESSION["cid"])){
    die("<script>location.href='index.php'</script>");
}
?>
<link type="text/css" href="<?php echo "$PATH/base/"; ?>ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo "$PATH/"; ?>jquery-1.3.2.js"></script>
<script type="text/javascript" src="<?php echo "$PATH/"; ?>ui/ui.core.js"></script>
<script type="text/javascript" src="<?php echo "$PATH/"; ?>ui/ui.datepicker.js"></script>
<script type="text/javascript" src="<?php echo "$PATH/"; ?>ui/i18n/ui.datepicker-id.js"></script>

<script type="text/javascript">
$(document).ready(function () {
    $("#tanggal").datepicker({
        dateFormat: "dd MM yy",
        changeMonth: true,
        changeYear: true
    });
});
</script>

<script type="text/javascript">
function PRINT() {
    win = window.open('mastervendor/print.php', 'win', 'width=1000, height=400, menubar=0, scrollbars=1, resizable=0, location=0, toolbar=0, pic_vendor=0');
}
</script>
<script language="JavaScript">
function buka(url) {
    window.open(url, 'window_baru', 'width=800,height=600,left=320,top=100,resizable=1,scrollbars=1');
}
</script>

<?php
$sql = "select `id_vendor` from `$tbmastervendor` order by `id_vendor` desc";
$q = mysqli_query($conn, $sql);
$jum = mysqli_num_rows($q);
$th = date("y");
$bl = date("m") + 0;
if ($bl < 10) {
    $bl = "0" . $bl;
}

$kd = "VEN" . $th . $bl; //KEG1610001
if ($jum > 0) {
    $d = mysqli_fetch_array($q);
    $idmax = $d["id_vendor"];

    $bul = substr($idmax, 5, 2);
    $tah = substr($idmax, 3, 2);
    if ($bul == $bl && $tah == $th) {
        $urut = substr($idmax, 7, 3) + 1;
        if ($urut < 10) {
            $idmax = "$kd" . "00" . $urut;
        } else if ($urut < 100) {
            $idmax = "$kd" . "0" . $urut;
        } else {
            $idmax = "$kd" . $urut;
        }
    }//==
    else {
        $idmax = "$kd" . "001";
    }
}//jum>0
else {
    $idmax = "$kd" . "001";
}
$id_vendor = $idmax;
?>

<?php
if ($_GET["pro"] == "ubah") {
    $id_vendor = $_GET["kode"];
    $sql = "select * from `$tbmastervendor` where `id_vendor`='$id_vendor'";
    $d = getField($conn, $sql);
    $id_vendor = $d["id_vendor"];
    $id_vendor0 = $d["id_vendor"];
    $nama_vendor = $d["nama_vendor"];
    $alamat_vendor = $d["alamat_vendor"];
    $telepon_vendor = $d["telepon_vendor"];
    $email_vendor = $d["email_vendor"];
    $pic_vendor = $d["pic_vendor"];
    $pro = "ubah";
}
?>


<form action="" method="post" enctype="multipart/form-data">
    <div class="box-body row">
        <div class="col-sm-3">
        </div>
        <div class="form-group col-sm-6" >
            <div class="form-group">
                <label for="id_vendor">ID Vendor</label>
                <input disabled="disabled" class="form-control" value="<?php echo $id_vendor; ?>"/>
            </div>
            <div class="form-group">
                <label for="nama_vendor">Nama Vendor</label>
                <input class="form-control" name="nama_vendor" type="text" id="nama_vendor" required="required" value="<?php echo $nama_vendor; ?>" size="30" />
            </div>
            <div class="form-group">
                <label for="alamat_vendor">Alamat Vendor</label>
                <textarea class="form-control" name="alamat_vendor" type="text" id="alamat_vendor" size="25"><?php echo $alamat_vendor; ?></textarea>
            </div>
            <div class="form-group">
                <label for="telepon_vendor">Telepon Vendor</label>
                <input class="form-control" name="telepon_vendor" type="number" id="telepon_vendor" required="required" value="<?php echo $telepon_vendor; ?>" size="30" />
            </div>
            <div class="form-group">
                <label for="email_vendor">Email Vendor</label>
                <input class="form-control" name="email_vendor" type="email" id="email_vendor" value="<?php echo $email_vendor; ?>" size="25" />
            </div>
            <div class=form-group>
                <label for="pic_vendor">PIC Vendor</label>
                <select class="form-control" name="pic_vendor" id="pic_vendor">
                    <?php
                    $sql = "select id_user,nama_user from `$tbmasterpetugas` where `level_user`='PIC'";
                    $arr = getData($conn, $sql);
                    foreach ($arr as $d) {
                        $id_user0 = $d["id_user"];
                        $nama_user = $d["nama_user"];
                        echo"<option value='$id_user0' ";
                        if ($id_user0 == $id_user) {
                            echo"selected";
                        }echo">$nama_user</option>";
                    }
                    ?>
                </select>
            </div><br/>
            <div class="form-group" align="right">
                <a href="?mnu=pvendor"><button type="button" name="Batal" id="Batal" class="btn btn-danger">Batal</button></a>
            </div>
        </div>
        <div class="col-sm-3">
        </div>
    </div>
</form>



<?php
if (isset($_POST["Simpan"])) {
    $pro = strip_tags($_POST["pro"]);
    $id_vendor = strip_tags($_POST["id_vendor"]);
    $id_vendor0 = strip_tags($_POST["id_vendor0"]);
    $nama_vendor = strip_tags($_POST["nama_vendor"]);
    $alamat_vendor = strip_tags($_POST["alamat_vendor"]);
    $telepon_vendor = strip_tags($_POST["telepon_vendor"]);
    $email_vendor = strip_tags($_POST["email_vendor"]);
    $pic_vendor = strip_tags($_POST["pic_vendor"]);

    if ($pro == "simpan") {
        $sql = " INSERT INTO `$tbmastervendor` (
            `id_vendor` ,
            `nama_vendor` ,
            `alamat_vendor` ,
            `telepon_vendor` ,
            `email_vendor` ,
            `pic_vendor`
        ) VALUES (
            '$id_vendor',
            '$nama_vendor',
            '$alamat_vendor',
            '$telepon_vendor',
            '$email_vendor',
            '$pic_vendor'
        )";

        $simpan = process($conn, $sql);
        if ($simpan) {
            echo "<script>alert('Data $id_vendor berhasil disimpan !');document.location.href='?mnu=mastervendor';</script>";
        } else {
            echo"<script>alert('Data $id_vendor gagal disimpan...');document.location.href='?mnu=mastervendor';</script>";
        }
    } else {
        $sql = "update `$tbmastervendor` set
        `nama_vendor`='$nama_vendor',
        `alamat_vendor`='$alamat_vendor' ,
        `telepon_vendor`='$telepon_vendor',
        `pic_vendor`='$pic_vendor',
        `email_vendor`='$email_vendor'
        where `id_vendor`='$id_vendor0'";
        $ubah = process($conn, $sql);
        if ($ubah) {
            echo "<script>alert('Data $id_vendor berhasil diubah !');document.location.href='?mnu=mastervendor';</script>";
        } else {
            echo"<script>alert('Data $id_vendor gagal diubah...');document.location.href='?mnu=mastervendor';</script>";
        }
    }//else simpan
}
?>
<?php
if ($_GET["pro"] == "hapus") {
    $id_vendor = $_GET["kode"];
    $sql = "delete from `$tbmastervendor` where `id_vendor`='$id_vendor'";
    $hapus = process($conn, $sql);
    if ($hapus) {
        echo "<script>alert('Data vendor $id_vendor berhasil dihapus !');document.location.href='?mnu=mastervendor';</script>";
    } else {
        echo"<script>alert('Data vendor $id_vendor gagal dihapus...');document.location.href='?mnu=mastervendor';</script>";
    }
}
?>
