<?php
require_once"../koneksivar.php";

$conn = new mysqli($DBServer, $DBUser, $DBPass, $DBName);
if ($conn->connect_error) {
  trigger_error('Database connection failed: '  . $conn->connect_error, E_USER_ERROR);
}

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

define('FPDF_FONTPATH', '../ypathcss/bantuan/fpdf/font/');
require('../ypathcss/bantuan/fpdf/fpdf.php');

class PDF extends FPDF{
  function Header(){
    $this->SetTextColor(128,0,0);
    $this->SetFont('Arial','B','12');//	$this->SetFont('Times','',12);
    $this->Cell(20,0,'Data transdata_p2tl',0,0,'L');
    $this->Ln();
    $this->Cell(5,1,'Laporan data transdata_p2tl',0,0,'L');
    $this->Ln();



  }

  function Footer(){
	$this->SetY(-4,5);
    $this->SetY(-2,5);
    $this->Cell(0,1,$this->PageNo(),0,0,'C');

  }
}

$sql = "select * from `$tbtransdata_p2tl`";
$jml =  getJum($conn,$sql);

$i=0;
$arr=getData($conn,$sql);
		foreach($arr as $d) {
  $cell[$i][0]=$d["id_transdata_p2tl"];
  $cell[$i][1]=$d["nomor_targetoperasi"];
  $cell[$i][2]=$d["tanggal_targetoperasi"];
  $cell[$i][3]=$d["id_user"];
  $cell[$i][4]=$d["id_pelanggan"];
  $cell[$i][5]=$d["id_vendor"];
  $cell[$i][6]=$d["nomor_workorder"];
  $cell[$i][7]=$d["tanggal_workorder"];
  $cell[$i][8]=$d["tanggal_respon_workorder"];
  $cell[$i][9]=$d["petugas_penerima"];
  $cell[$i][10]=$d["latitude"];
  $cell[$i][11]=$d["longitude"];
  $cell[$i][12]=$d["foto_1"];
  $cell[$i][13]=$d["foto_2"];
  $cell[$i][14]=$d["foto_3"];
  $cell[$i][15]=$d["foto_4"];
  $cell[$i][16]=$d["foto_5"];
  $cell[$i][17]=$d["foto_6"];
  $cell[$i][18]=$d["petugas_lapangan"];
  $cell[$i][19]=$d["cek_pelanggaran"];
  $cell[$i][20]=$d["kode_pelanggaran"];
  $cell[$i][21]=$d["tanggal_penertiban"];
  $cell[$i][22]=$d["status_pelaksanaan"];
  $i++;
}


$pdf=new PDF('L','cm','A3');
//$pdf=new PDF("P","in","Letter");
$pdf->Open();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','B','9');
$pdf->SetFillColor(192,192,192);
$pdf->Cell(1,1,'no','LR',0,'L',1);
//$pdf->MultiCell(0, 0.5, $lipsum1, 'LR', "L");
$pdf->Cell(2,1,'id_transdata_p2tl','LR',0,'C',1);
$pdf->Cell(3,1,'nomor_targetoperasi','LR',0,'C',1);
$pdf->Cell(3,1,'tanggal_targetoperasi','LR',0,'C',1);
$pdf->Cell(3,1,'id_user','LR',0,'C',1);
$pdf->Cell(3,1,'id_pelanggan','LR',0,'C',1);
$pdf->Cell(3,1,'id_vendor','LR',0,'C',1);
$pdf->Cell(3,1,'nomor_workorder','LR',0,'C',1);
$pdf->Cell(3,1,'tanggal_workorder','LR',0,'C',1);
$pdf->Cell(3,1,'tanggal_respon_workorder','LR',0,'C',1);
$pdf->Cell(3,1,'petugas_penerima','LR',0,'C',1);
$pdf->Cell(3,1,'latitude','LR',0,'C',1);
$pdf->Cell(3,1,'longitude','LR',0,'C',1);
$pdf->Cell(3,1,'foto_1','LR',0,'C',1);
$pdf->Cell(3,1,'foto_2','LR',0,'C',1);
$pdf->Cell(3,1,'foto_3','LR',0,'C',1);
$pdf->Cell(3,1,'foto_4','LR',0,'C',1);
$pdf->Cell(3,1,'foto_5','LR',0,'C',1);
$pdf->Cell(3,1,'foto_6','LR',0,'C',1);
$pdf->Cell(3,1,'petugas_lapangan','LR',0,'C',1);
$pdf->Cell(3,1,'cek_pelanggaran','LR',0,'C',1);
$pdf->Cell(3,1,'kode_pelanggaran','LR',0,'C',1);
$pdf->Cell(3,1,'tanggal_penertiban','LR',0,'C',1);
$pdf->Cell(3,1,'status_pelaksanaan','LR',0,'C',1);
$pdf->Ln();
$pdf->SetFont('Arial','','8');

for ($j=0;$j<$i;$j++){
  $pdf->Cell(1,1,$j+1,'B',0,'L');         // no
  $pdf->Cell(2,1,$cell[$j][0],'B',0,'L'); // id_transdata_p2tl
  $pdf->Cell(7,1,$cell[$j][1],'B',0,'L'); // nomor_targetoperasi
  $pdf->Cell(5,1,$cell[$j][2],'B',0,'L'); // tanggal_targetoperasi
  $pdf->Cell(3,1,$cell[$j][3],'B',0,'L'); // id_user
  $pdf->Cell(3,1,$cell[$j][4],'B',0,'L'); // id_pelanggan
  $pdf->Cell(3,1,$cell[$j][5],'B',0,'L'); // id_vendor
  $pdf->Cell(3,1,$cell[$j][6],'B',0,'L'); // nomor_workorder
  $pdf->Cell(3,1,$cell[$j][7],'B',0,'L'); // tanggal_workorder
  $pdf->Cell(9,1,$cell[$j][8],'B',0,'L'); // tanggal_respon_workorder
  $pdf->Cell(1,1,$cell[$j][9],'B',0,'L'); // status_pelaksanaan
  $pdf->Ln();
}
$pdf->Output();
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

function getJum($conn,$sql){
  $rs=$conn->query($sql);
  $jum= $rs->num_rows;
	$rs->free();
	return $jum;
}

function getData($conn,$sql){
	$rs=$conn->query($sql);
	$rs->data_seek(0);
	$arr = $rs->fetch_all(MYSQLI_ASSOC);

	$rs->free();
	return $arr;
}
?>
