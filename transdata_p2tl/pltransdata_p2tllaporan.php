<?php
$pro = "simpan";
$tanggal_workorder = (date("Y-m-d H:i:s"));
$tanggal_targetoperasi = (date("Y-m-d H:i:s"));
$tanggal_respon_workorder = (date("Y-m-d H:i:s"));
$tanggal_penertiban = (date("Y-m-d H:i:s"));
$tanggal_berangkat = (date("Y-m-d H:i:s"));
$tanggal_sampai = (date("Y-m-d H:i:s"));
$foto_10 = "avatar.jpg";
$foto_20 = "avatar.jpg";
$foto_30 = "avatar.jpg";
$foto_40 = "avatar.jpg";
$foto_50 = "avatar.jpg";
$foto_60 = "avatar.jpg";

if(!isset($_SESSION["cid"])){
    die("<script>location.href='index.php'</script>");
}
?>
<link type="text/css" href="<?php echo "$PATH/base/"; ?>ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo "$PATH/"; ?>jquery-1.3.2.js"></script>
<script type="text/javascript" src="<?php echo "$PATH/"; ?>ui/ui.core.js"></script>
<script type="text/javascript" src="<?php echo "$PATH/"; ?>ui/ui.datepicker.js"></script>
<script type="text/javascript" src="<?php echo "$PATH/"; ?>ui/i18n/ui.datepicker-id.js"></script>

<script type="text/javascript">

    $(document).ready(function () {
    $('.tanggal_penertiban').datetimepicker({
          format: 'yyyy-mm-dd hh:ii:ss',
           weekStart: 1,
           todayBtn:  1,
           autoclose: 1,
           todayHighlight: 1,
           startView: 2,
           forceParse: 0,
           showMeridian: 1
       });
    });

</script>

<?php
$sql = "select `id_transdata_p2tl` from `$tbtransdata_p2tl` order by `id_transdata_p2tl` desc";
$q = mysqli_query($conn, $sql);
$jum = mysqli_num_rows($q);
$th = date("y");
$bl = date("m") + 0;
if ($bl < 10) {
    $bl = "0" . $bl;
}

$kd = "TDP" . $th . $bl; //KEG1610001
if ($jum > 0) {
    $d = mysqli_fetch_array($q);
    $idmax = $d["id_transdata_p2tl"];

    $bul = substr($idmax, 5, 2);
    $tah = substr($idmax, 3, 2);
    if ($bul == $bl && $tah == $th) {
        $urut = substr($idmax, 7, 3) + 1;
        if ($urut < 10) {
            $idmax = "$kd" . "00" . $urut;
        } else if ($urut < 100) {
            $idmax = "$kd" . "0" . $urut;
        } else {
            $idmax = "$kd" . $urut;
        }
    }//==
    else {
        $idmax = "$kd" . "001";
    }
}//jum>0
else {
    $idmax = "$kd" . "001";
}
$id_transdata_p2tl = $idmax;
?>

<?php
if ($_GET["pro"] == "ubah") {
    $id_transdata_p2tl = $_GET["kode"];
    $sql = "select * from `$tbtransdata_p2tl` where `id_transdata_p2tl`='$id_transdata_p2tl'";
    $d = getField($conn, $sql);
    $id_transdata_p2tl = $d["id_transdata_p2tl"];
    $id_transdata_p2tl0 = $d["id_transdata_p2tl"];
    $nomor_targetoperasi = $d["nomor_targetoperasi"];
    $tanggal_targetoperasi = $d["tanggal_targetoperasi"];
    $id_user = $d["id_user"];
    $id_pelanggan = $d["id_pelanggan"];
    $id_vendor = $d["id_vendor"];
    $nomor_workorder = ($d["nomor_workorder"]);
    $tanggal_workorder = ($d["tanggal_workorder"]);
    $tanggal_respon_workorder = $d["tanggal_respon_workorder"];
    $petugas_penerima = ($d["petugas_penerima"]);
    $latitude = ($d["latitude"]);
    $longitude = ($d["longitude"]);
    $foto_1 = ($d["foto_1"]);
    $foto_10 = ($d["foto_1"]);
    $foto_2 = ($d["foto_2"]);
    $foto_20 = ($d["foto_2"]);
    $foto_3 = ($d["foto_3"]);
    $foto_30 = ($d["foto_3"]);
    $foto_4 = ($d["foto_4"]);
    $foto_40 = ($d["foto_4"]);
    $foto_5 = ($d["foto_5"]);
    $foto_50 = ($d["foto_5"]);
    $foto_6 = ($d["foto_6"]);
    $foto_60 = ($d["foto_6"]);
    $petugas_lapangan = ($d["petugas_lapangan"]);
    $cek_pelanggaran = ($d["cek_pelanggaran"]);
    $kode_pelanggaran = ($d["kode_pelanggaran"]);
    $tanggal_penertiban = ($d["tanggal_penertiban"]);
    $status_pelaksanaan = $d["status_pelaksanaan"];
    $pro = "ubah";
}
?>


<form action="" method="post" enctype="multipart/form-data">
    <div class="box-body row">
        <div class="form-group col-sm-3" >
    <center>
            <?php
            echo"<a href='#' onclick='buka(\"transdata_p2tl/zoom.php?id=$id_transdata_p2tl\")'>
    <img src='$YPATH/$foto_10' width='77' height='80' />
    </a>
    ";
            echo"<a href='#' onclick='buka(\"transdata_p2tl/zoom2.php?id=$id_transdata_p2tl\")'>
        <img src='$YPATH/$foto_20' width='77' height='80' />
        </a>
        ";
            echo"<a href='#' onclick='buka(\"transdata_p2tl/zoom3.php?id=$id_transdata_p2tl\")'>
        <img src='$YPATH/$foto_30' width='77' height='80' />
        </a>
        ";
            echo"<a href='#' onclick='buka(\"transdata_p2tl/zoom4.php?id=$id_transdata_p2tl\")'>
        <img src='$YPATH/$foto_40' width='77' height='80' />
        </a>
        ";
            echo"<a href='#' onclick='buka(\"transdata_p2tl/zoom5.php?id=$id_transdata_p2tl\")'>
        <img src='$YPATH/$foto_50' width='77' height='80' />
        </a>
        ";
                echo"<a href='#' onclick='buka(\"transdata_p2tl/zoom6.php?id=$id_transdata_p2tl\")'>
        <img src='$YPATH/$foto_60' width='77' height='80' />
        </a>
        ";
            ?>
    </center>
        </div>
        <div class="form-group col-sm-1" >
        </div>
        <div class="form-group col-sm-4" >
            <div class="form-group">
                <label for="id_transdata_p2tl">ID Transaksi Data</label>
                <input disabled="disabled" class="form-control" value="<?php echo $id_transdata_p2tl; ?>"/>
            </div>
            <div class="form-group">
                <label for="nomor_targetoperasi">Nomor Target Operasi</label>
                <input class="form-control" disabled="disabled" name="nomor_targetoperasi" required="required" type="text" id="nomor_targetoperasi" value="<?php echo $nomor_targetoperasi; ?>" size="30" />
            </div>

            <div class="form-group">
                <label for="nomor_workorder">Nomor Work Order</label>
                <input class="form-control" disabled="disabled" name="nomor_workorder" required="required" type="text" id="nomor_workorder" value="<?php echo $nomor_workorder; ?>" size="30" />
            </div>

            <div class="form-group">
                <label for="id_pelanggan">ID Pelanggan</label>
                <select class="form-control" disabled="disabled" name="id_pelanggan" id="id_pelanggan">
                    <?php
                    $sql = "select id_pelanggan,nama_pelanggan from `$tbpelanggan`";//" where `level_user`='PIC'";
                    $arr = getData($conn, $sql);
                    foreach ($arr as $d) {
                        $id_pelanggan0 = $d["id_pelanggan"];
                        $nama_pelanggan = $d["nama_pelanggan"];
                        echo"<option value='$id_pelanggan0' ";
                        if ($id_pelanggan0 == $id_pelanggan) {
                            echo"selected";
                        }echo">$id_pelanggan0</option>";
                    }
                    ?>
                </select>
            </div>

            <div class="form-group">
                <label for="id_pelanggan">Nama Pelanggan</label>
                <select class="form-control" disabled="disabled" name="id_pelanggan" id="id_pelanggan">
                    <?php
                    $sql = "select id_pelanggan,nama_pelanggan from `$tbpelanggan`";//" where `level_user`='PIC'";
                    $arr = getData($conn, $sql);
                    foreach ($arr as $d) {
                        $id_pelanggan0 = $d["id_pelanggan"];
                        $nama_pelanggan = $d["nama_pelanggan"];
                        echo"<option value='$id_pelanggan0' ";
                        if ($id_pelanggan0 == $id_pelanggan) {
                            echo"selected";
                        }echo">$nama_pelanggan</option>";
                    }
                    ?>
                </select>
            </div>

            <div class="form-group">
                <label for="alamat_pelanggan">Alamat</label>
                <select class="form-control" disabled="disabled" name="alamat_pelanggan" id="alamat_pelanggan">
                    <?php
                    $sql = "select id_pelanggan,alamat_pelanggan from `$tbpelanggan`";//" where `level_user`='PIC'";
                    $arr = getData($conn, $sql);
                    foreach ($arr as $d) {
                        $id_pelanggan0 = $d["id_pelanggan"];
                        $alamat_pelanggan = $d["alamat_pelanggan"];

                        echo"<option value='$id_pelanggan0' ";
                        if ($id_pelanggan0 == $id_pelanggan) {
                            echo"selected";
                        }echo">$alamat_pelanggan</option>";
                    }
                    ?>
                </select>
            </div>

            <div class="form-group">
                <label for="tarif">Tarif / Daya</label>
                <select class="form-control" disabled="disabled" name="tarif" id="tarif">
                    <?php
                    $sql = "select id_pelanggan,tarif,daya from `$tbpelanggan`";//" where `level_user`='PIC'";
                    $arr = getData($conn, $sql);
                    foreach ($arr as $d) {
                        $id_pelanggan0 = $d["id_pelanggan"];
                        $tarif = $d["tarif"];
                        $daya = $d["daya"];
                        echo"<option value='$id_pelanggan0' ";
                        if ($id_pelanggan0 == $id_pelanggan) {
                            echo"selected";
                        }echo">$tarif / $daya</option>";
                    }
                    ?>
                </select>
            </div>

            <div class="form-group">
                <label for="latitude">Latitude</label>
                <input class="form-control" name="latitude" required="required" type="text" id="latitude" value="<?php echo $latitude; ?>" size="30" />
            </div>
            <div class="form-group">
                <label for="longitude">Longitude</label>
                <input class="form-control" name="longitude" required="required" type="text" id="longitude" value="<?php echo $longitude; ?>" size="30" />
            </div>
            <div class="form-group">
                <label for="foto_1">Foto 1</label>
                <input class="form-control" name="foto_1" type="file" id="foto_1" size="30" />
                <i class="fa fa-image"></i> <a href='#' onclick='buka("transdata_p2tl/zoom.php?id=<?php echo $id_transdata_p2tl; ?>")'><?php echo $foto_10; ?></a>
            </div>
            <div class="form-group">
                <label for="foto_1">Foto 2</label>
                <input class="form-control" name="foto_2" type="file" id="foto_2" size="30" />
                <i class="fa fa-image"></i> <a href='#' onclick='buka("transdata_p2tl/zoom2.php?id=<?php echo $id_transdata_p2tl; ?>")'><?php echo $foto_20; ?></a>
            </div>
            <div class="form-group">
                <label for="foto_3">Foto 3</label>
                <input class="form-control" name="foto_3"  type="file" id="foto_3" size="30" />
                <i class="fa fa-image"></i> <a href='#' onclick='buka("transdata_p2tl/zoom3.php?id=<?php echo $id_transdata_p2tl; ?>")'><?php echo $foto_30; ?></a>
            </div>

        </div>
        <div class="form-group col-sm-4" >
            <div class="form-group">
                <label for="foto_4">Foto 4</label>
                <input class="form-control" name="foto_4" type="file" id="foto_4" size="30" />
                <i class="fa fa-image"></i> <a href='#' onclick='buka("transdata_p2tl/zoom4.php?id=<?php echo $id_transdata_p2tl; ?>")'><?php echo $foto_40; ?></a>
            </div>
            <div class="form-group">
                <label for="foto_5">Foto 5</label>
                <input class="form-control" name="foto_5" type="file" id="foto_5" size="30" />
                <i class="fa fa-image"></i> <a href='#' onclick='buka("transdata_p2tl/zoom5.php?id=<?php echo $id_transdata_p2tl; ?>")'><?php echo $foto_50; ?></a>
            </div>
            <div class="form-group">
                <label for="foto_6">Foto 6</label>
                <input class="form-control" name="foto_6" type="file" id="foto_6" size="30" />
                <i class="fa fa-image"></i> <a href='#' onclick='buka("transdata_p2tl/zoom6.php?id=<?php echo $id_transdata_p2tl; ?>")'><?php echo $foto_60; ?></a>
            </div>

            <div class="form-group">
                <label for="cek_pelanggaran">Cek Pelanggaran</label>
                <select class="form-control" name="cek_pelanggaran" id="cek_pelanggaran">
                    <option value="">Silahkan Pilih</option>
                    <option value="ya">Ya</option>
                    <option value="tidak">Tidak</option>
                </select>
            </div>
            <div class="form-group">
                <label for="kode_pelanggaran">Kode Pelanggaran</label>
                <input class="form-control" name="kode_pelanggaran" required="required" type="text" id="kode_pelanggaran" value="<?php echo $kode_pelanggaran; ?>" size="30" />
            </div>
            <div class="form-group">
                <label for="tanggal_penertiban">Tanggal Penertiban</label>
                <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input class="form-control date tanggal_penertiban" data-date-format="yyyy-mm-dd HH:ii:ss" name="tanggal_penertiban" required="required" type="text" id="tanggal_penertiban" value="<?php echo $tanggal_penertiban; ?>" />
                </div>
            </div>
            <div class="form-group">
                <label for="status_pelaksanaan">Status Pelaksanaan</label>
                <select class="form-control" name="status_pelaksanaan" id="status_pelaksanaan">
                    <?php
                              $sql = "select id_status_pelaksanaan,nama_status from `$tbmasterstatus`";//where `level_user`='PL'";
                              $arr = getData($conn, $sql);
                              foreach ($arr as $d) {
                                  $id_status_pelaksanaan0 = $d["id_status_pelaksanaan"];
                                  $nama_status = $d["nama_status"];
                                  echo"<option value='$id_status_pelaksanaan0' ";
                                  if ($id_status_pelaksanaan0 == $id_status_pelaksanaan) {
                                      echo"selected";
                                  }echo">$nama_status</option>";
                              }
                              ?>
                </select>  </div>
            <div class="form-group" align="right">
                <button type="submit" name="Simpan"id="Simpan" class="btn btn-primary">Simpan</button>
                <input name="pro" type="hidden" id="pro" value="<?php echo $pro; ?>" />
                <input name="foto_10" type="hidden" id="foto_10" value="<?php echo $foto_10; ?>" />
                <input name="foto_20" type="hidden" id="foto_20" value="<?php echo $foto_20; ?>" />
                <input name="foto_30" type="hidden" id="foto_30" value="<?php echo $foto_30; ?>" />
                <input name="foto_40" type="hidden" id="foto_40" value="<?php echo $foto_40; ?>" />
                <input name="foto_50" type="hidden" id="foto_50" value="<?php echo $foto_50; ?>" />
                <input name="foto_60" type="hidden" id="foto_60" value="<?php echo $foto_60; ?>" />
                <input name="id_transdata_p2tl" type="hidden" id="id_transdata_p2tl" value="<?php echo $id_transdata_p2tl; ?>" />
                <input name="id_transdata_p2tl0" type="hidden" id="id_transdata_p2tl0" value="<?php echo $id_transdata_p2tl0; ?>" />
                <a href="?mnu=transdata_p2tl"><button type="button" name="Batal" id="Batal" class="btn btn-danger">Batal</button></a>
            </div>
        </div>
    </div>
</form>

<?php
if (isset($_POST["Simpan"])) {
    $pro = strip_tags($_POST["pro"]);
    $id_transdata_p2tl = strip_tags($_POST["id_transdata_p2tl"]);
    $id_transdata_p2tl0 = strip_tags($_POST["id_transdata_p2tl0"]);
    $nomor_targetoperasi = strip_tags($_POST["nomor_targetoperasi"]);
    $tanggal_targetoperasi = strip_tags($_POST["tanggal_targetoperasi"]);
    $id_user = strip_tags($_POST["id_user"]);
    $id_pelanggan = strip_tags($_POST["id_pelanggan"]);
    $id_vendor = strip_tags($_POST["id_vendor"]);
    $nomor_workorder = strip_tags($_POST["nomor_workorder"]);
    $tanggal_workorder = (strip_tags($_POST["tanggal_workorder"]));
    $tanggal_respon_workorder = strip_tags($_POST["tanggal_respon_workorder"]);
    $petugas_penerima = strip_tags($_POST["petugas_penerima"]);
    $latitude = strip_tags($_POST["latitude"]);
    $longitude = strip_tags($_POST["longitude"]);

    $foto_10 = strip_tags($_POST["foto_10"]);
    if ($_FILES["foto_1"] != "") {
        @copy($_FILES["foto_1"]["tmp_name"], "$YPATH/" . $_FILES["foto_1"]["name"]);
        $foto_1 = $_FILES["foto_1"]["name"];
    } else {
        $foto_1 = $foto_10;
    }
    if (strlen($foto_1) < 1) {
        $foto_1 = $foto_10;
    }

    $foto_20 = strip_tags($_POST["foto_20"]);
    if ($_FILES["foto_2"] != "") {
    @copy($_FILES["foto_2"]["tmp_name"], "$YPATH/" . $_FILES["foto_2"]["name"]);
    $foto_2 = $_FILES["foto_2"]["name"];
    } else {
    $foto_2 = $foto_20;
    }
    if (strlen($foto_2) < 1) {
    $foto_2 = $foto_20;
    }

        $foto_30 = strip_tags($_POST["foto_30"]);
    if ($_FILES["foto_3"] != "") {
        @copy($_FILES["foto_3"]["tmp_name"], "$YPATH/" . $_FILES["foto_3"]["name"]);
        $foto_3 = $_FILES["foto_3"]["name"];
    } else {
        $foto_3 = $foto_30;
    }
    if (strlen($foto_3) < 1) {
        $foto_3 = $foto_30;
    }

        $foto_40 = strip_tags($_POST["foto_40"]);
    if ($_FILES["foto_4"] != "") {
        @copy($_FILES["foto_4"]["tmp_name"], "$YPATH/" . $_FILES["foto_4"]["name"]);
        $foto_4 = $_FILES["foto_4"]["name"];
    } else {
        $foto_4 = $foto_40;
    }
    if (strlen($foto_4) < 1) {
        $foto_4 = $foto_40;
    }

        $foto_50 = strip_tags($_POST["foto_50"]);
    if ($_FILES["foto_5"] != "") {
        @copy($_FILES["foto_5"]["tmp_name"], "$YPATH/" . $_FILES["foto_5"]["name"]);
        $foto_5 = $_FILES["foto_5"]["name"];
    } else {
        $foto_5 = $foto_50;
    }
    if (strlen($foto_5) < 1) {
        $foto_5 = $foto_50;
    }

        $foto_60 = strip_tags($_POST["foto_60"]);
    if ($_FILES["foto_6"] != "") {
        @copy($_FILES["foto_6"]["tmp_name"], "$YPATH/" . $_FILES["foto_6"]["name"]);
        $foto_6 = $_FILES["foto_6"]["name"];
    } else {
        $foto_6 = $foto_60;
    }
    if (strlen($foto_6) < 1) {
        $foto_6 = $foto_60;
    }

    $petugas_lapangan = strip_tags($_POST["petugas_lapangan"]);
    $cek_pelanggaran = strip_tags($_POST["cek_pelanggaran"]);
    $kode_pelanggaran = strip_tags($_POST["kode_pelanggaran"]);
    $tanggal_penertiban = strip_tags($_POST["tanggal_penertiban"]);
    $status_pelaksanaan = strip_tags($_POST["status_pelaksanaan"]);

    if ($pro == "simpan") {
        $sql = " INSERT INTO `$tbtransdata_p2tl` (
            `latitude` ,
            `longitude` ,
            `foto_1` ,
            `foto_2` ,
            `foto_3` ,
            `foto_4` ,
            `foto_5` ,
            `foto_6` ,
            `cek_pelanggaran` ,
            `kode_pelanggaran` ,
            `tanggal_penertiban` ,
            `status_pelaksanaan`
        ) VALUES (
            '$latitude',
            '$longitude',
            '$foto_1',
            '$foto_2',
            '$foto_3',
            '$foto_4',
            '$foto_5',
            '$foto_6',
            '$cek_pelanggaran',
            '$kode_pelanggaran',
            '$tanggal_penertiban',
            '$status_pelaksanaan'
        )";

        $simpan = process($conn, $sql);
        if ($simpan) {
            echo "<script>alert('Data $id_transdata_p2tl berhasil disimpan !');document.location.href='?mnu=pltransdata_p2tl';</script>";
        } else {
            echo"<script>alert('Data $id_transdata_p2tl gagal disimpan...');document.location.href='?mnu=pltransdata_p2tl';</script>";
        }
    } else {
        $sql = "update `$tbtransdata_p2tl` set
        `latitude`='$latitude',
        `longitude`='$longitude',
        `foto_1`='$foto_1',
        `foto_2`='$foto_2',
        `foto_3`='$foto_3',
        `foto_4`='$foto_4',
        `foto_5`='$foto_5',
        `foto_6`='$foto_6',
        `cek_pelanggaran`='$cek_pelanggaran',
        `kode_pelanggaran`='$kode_pelanggaran',
        `tanggal_penertiban`='$tanggal_penertiban',
        `status_pelaksanaan`='$status_pelaksanaan'
        where `id_transdata_p2tl`='$id_transdata_p2tl0'";
        $ubah = process($conn, $sql);
        if ($ubah) {
            echo "<script>alert('Data $id_transdata_p2tl berhasil diubah !');document.location.href='?mnu=pltransdata_p2tl';</script>";
        } else {
            echo"<script>alert('Data $id_transdata_p2tl gagal diubah...');document.location.href='?mnu=pltransdata_p2tl';</script>";
        }
    }//else simpan
}
?>

<?php
if ($_GET["pro"] == "hapus") {
    $id_transdata_p2tl = $_GET["kode"];
    $sql = "delete from `$tbtransdata_p2tl` where `id_transdata_p2tl`='$id_transdata_p2tl'";
    $hapus = process($conn, $sql);
    if ($hapus) {
        echo "<script>alert('Data to $id_transdata_p2tl berhasil dihapus !');document.location.href='?mnu=pltransdata_p2tl';</script>";
    } else {
        echo"<script>alert('Data to $id_transdata_p2tl gagal dihapus...');document.location.href='?mnu=pltransdata_p2tl';</script>";
    }
}
?>
